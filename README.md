# Jamulus server

## Controlling recordings

When using the recording function with the -Rcommand line option, if the server receives a SIGUSR1 signal during a recording, it will start a new recording in a new directory. SIGUSR2 will toggle recording enabled on/off.

```
# Start a new recording
docker kill -s SIGUSR1 jamulus

# Toggle recording on / off
docker kill -s SIGUSR2 jamulus
```
